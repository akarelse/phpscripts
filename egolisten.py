#!/usr/bin/python

import socket
import sys

import bz2
from struct import *
from Buffer import Buffer
import base64
from MessageDefs import MessageDefs

class MessageHandlers(MessageDefs):


	def handleMessageTag(self, message):
		print "-" + message['tag'] + "-" + message['message'] + "-" + message['tag'] + "-"

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to the port
server_address = ('localhost', 5510)
print >>sys.stderr, 'starting up on %s port %s' % server_address
sock.bind(server_address)
buff = Buffer()

mesghandlers = MessageHandlers()


# Listen for incoming connections
sock.listen(1)
print 'waiting for a connection'
while True:
	# Wait for a connection
	connection, client_address = sock.accept()
	try:
		while True:
			if not buff.add_from_connection(connection):
				break


		
			mesghandlers.handlemessage(buff)
			buff.discard()

			
	finally:
		pass
		# Clean up the connection

connection.close()