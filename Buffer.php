<?php

class Buffer implements Countable
{
	public $buff = "";
    private $pos = 0;

    public function __construct($buffy=0)
    {
        if ($buffy != 0)
        {
            $this->buff = $buffy;
        }
    }

    public function count()
    {
        return strlen($this->buff) - $this->pos;
    }

    public function __toString()
    {
        return $this->buff;
    }

    public function add_from_connection($connection)
    {
        $rawlength = "";
        $check = "";
        if (socket_recv($connection, $rawlength, 4) < 1)
        {
            return false;
        }
        $length = unpack("l", $rawlength);
        socket_recv($connection, $this->buff, $length);
        socket_recv($connection, $check, 32);
        return ($check == md5($this->buff));
    }


    public function add_end($data)
    {
        $this->buff .= $data;
    }

    public function add_begin($data)
    {
        $this->buff = $data . $this->buff;
    }


    public function save()
    {
        $this->buff = array_slice($this->buff,$this->pos);
        $this->pos = 0;
    }

    public function restore()
    {
    	$this->pos = 0;
    }

    public function discard()
    {
        $this->pos = count($this->buff);
    }

    public function read($length=null)
    {
        if (empty($length))
        {
            $data = array_slice($this->buff, $this->pos);
            $this->pos = count($this->buff);
        }
        else
        {
            if ($this->pos + $length > count($this->buff))
            {
                throw Exception("Buffer underrun !");
            }

            $data = array_slice($this->buff, $this->pos, $this->pos+$thing);
            $this->pos += $length;
        }

        return $data;
    }

    public function unpack($fmt)
    {
        $fmt = ">" + $fmt;
        $length = $this->calcsize($fmt);
        $fields = unpack($fmt, $this->read($length));
        if (count($fields) == 1)
        {
            $fields = $fields[0];
        }
        return $fields;
    }

    public function unpack_solid_string_as_buffer()
    {
        $buff = new Buffer();
        $buff->add_end($this->buff);
        return $buff;
    }

    public function unpack_bzip_as_buffer()
    {
        $buff = new Buffer();
        $buff->add_end(bzdecompress($this->read()));
        return $buff;
    }

    public function unpack_solid_string()
    {
    	$length = $this->unpack('N');
    	return $this->read($length);
    }

    public function pack_solid_string($string)
	{
		$length = strlen($string);

		$this->add_end($this->pack('N', $length));
		$this->add_end($string);
        return $this;
	}

    public function pack_char($num)
    {
        $this->add_end($this->pack('C', $num));
        return $this;
    }    

    public function add_uname($uname)
    {
        $length = strlen($uname);
        $this->add_begin($uname);
        $this->add_begin($this->pack('C', $length));
        return $this;
    }

    public function unpack_uname()
    {
        $buff = new Buffer();
        $length = $this->unpack('C');
        $buff->add_end($this->read($length));
    }

	public function pack_tag($tag)
	{
		$length = strlen($tag);
		$this->add_end($this->pack('C', $length));
		$this->add_end($tag);
        return $this;
	}

    public function pack_tags($tags)
    {
        if (!is_array($tags))
        {
            return;
        }
        $length = count($tags);
        $this->add_end($this->pack('C', $length));
        foreach ($tags as $value)
        {
            $this->pack_tag($value);
        }
        return $this;
    }

    public function add_checksum($islast=false)
    {
        $reststring = substr($this->buff,4);     
        $this->add_end(md5($reststring));
        return $this;
    }

    public function add_length()
    {

        $this->add_begin($this->pack('N', strlen($this->buff)));
        return $this;
    }

    public function add_mesg_num($num)
    {
        $this->add_begin($this->pack('C', $num));
        return $this;
    }

    public function pack_as_bzip_buffer()
    {
        $buff = new Buffer();
        $temp = bzcompress($this->buff);
        $buff->add_end(bzcompress($this->buff));
        return $buff;
    }

    public function unpack_tag()
    {
        $length = $this->unpack('C');
        return $this->read($length);
    }

    private function calcsize($fmt)
    {
    	$size = 0;
    	$charstostrip = array("<", ">", "*");
    	$fmt = str_replace($charstostrip,"", $fmt);
    	list($alpha,$numeric) = sscanf($example, "%[A-Z]%d");
    	$table = array('c'=>1,'C'=>1,'s'=>2,'S'=>2,'n'=>2,'v'=>2,'l'=>4,'L'=>4,'N'=>4,'V'=>4,'q'=>8,'Q'=>8,'J'=>8,'P'=>8);
    	
    	foreach (str_repeat($alpha, $numeric) as $value)
    	{
    		$size += $table[$value];
    	}
		return $size;   	
    }


   	public function pack($fmt, $data)
   	{
   		return pack($fmt, $data);
   	}

}