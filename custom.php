<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');

include_once PATH_CONFIG . '../../../phpscripts/egoDecorateModel.php';
include_once PATH_CONFIG . '../../../phpscripts/egolog.php';


require PATH_CONFIG . 'development.php';
class CustomConfig extends DevelopmentConfig
{
	public function __construct()
	{

		parent::__construct();
		egostart(5510, "localhost");

		// $this->_strDbName ='naris3-live';
		$this->_strDbHost = 'pg';
		$this->_intDbPort = 5432;


		// $this->_strDbName = 'naris3-tnt-cb';
		$this->_strDbName = 'naris3-develop';
		

		$this->_strCachePath = getcwd() . '/cache';

		/* test email adress */
		$this->_strTestEmailAddress = 'a.karelse@naris.com';


		/* error reporting */
		// $this->_intErrorReporting = E_ERROR & E_PARSE & ~E_DEPRECATED;
		$this->_intErrorReporting = E_ALL;
		// $this->_intErrorReporting = E_ERROR;

		//Authentication Two factor
		$this->_blnAtfEnabled = false;
		
		$this->_blnAlEnabled = true;

		/**
		 * set the error handling output in developement environment
		 * default: set to php output
		 * FB: when firebug enabled, set to Firebug
		 * file: set to cache/error_log file
		 */
		$this->_strErrorHandling = 'default';
		// $this->_strErrorHandling = 'FB';
		// $this->_strErrorHandling = 'file';

		// $this->_intSessionExpiration = 10 * 3600;
		$this->_blnXssEnabled = true;

		/**
		 * if XHprof installed, and in developement environment
		 * the xhprof will be called within naris.
		 */
		// $this->_blnUseXhProf = true;


		// $this->_strDefaultTheme = 'CB';
		// setlocale(LC_CTYPE, 0);
		//setlocale(LC_CTYPE, "C");


		$this->_strCommunicationProtocol = 'http';
		$this->_strDomainName = 'dev01';

	}
}
