
from Buffer import Buffer

class MessageDefs(object):
	inboundmessages = {}
	send_name = "";

	def __init__(self):
		self.inboundmessages.update({1:self.decodeMessageTag,2:self.decodeMd5Avalable,3:self.decodeAskNames,4:self.decodeAskBoolean})

	def handlemessage(self, buff):
		mesgtype = buff.unpack('B')
		self.send_name = buff.unpack_uname()

		if mesgtype in self.inboundmessages:
			self.inboundmessages[mesgtype](buff)


	# DECODING
	
	def decodeMessageTag(self, data):
		data = data.unpack_bzip_as_buffer()
		message = {"message":str(data.unpack_solid_string()),"tag":str(data.unpack_tag())}
		self.handleMessageTag(message)	

	def decodeMd5Avalable(self, data):
		message = {"md5":str(data.unpack_solid_string())}
		self.handleMd5Avalable(message)

	def decodeAskNames(self, data):
		self.handleAskName()

	def decodeAskBoolean(self, data):
		num, boellie = data.unpack_num_bool()
		message = {"mesg_num":str(num),"bool":str(boellie)}
		self.handleAskBoolean(message)


	# ENCODING 

	def encodeMessageTag(self, data):	
		buff = Buffer()
		newbuff = buff.pack_solid_string(data['message']).pack_tag(data['tag']).pack_as_bzip_buffer().add_uname(self.send_name).add_mesg_num(1).add_length().add_checksum();
		return newbuff

	def encodeMd5Avalable(self, data):
		buff = Buffer()
		newbuff = buff.pack_solid_string(data['md5']).add_uname(self.send_name).add_mesg_num(2).add_length().add_checksum();

	def encodeAskNames(self, data):
		buff = Buffer()
		newbuff = buff.add_uname(self.send_name).add_mesg_num(3).add_length().add_checksum();

	def encodeAskBoolean(self, data):
		buff = Buffer()
		newbuff = buff.pack_num_bool.add_uname(self.send_name).add_mesg_num(4).add_length().add_checksum();

	

	# HANDELING

	def handleMessageTag(self, message):
		pass

	def handleMd5Avalable(self, message):
		pass

	def handleAskNames(self, message):
		pass

	def handleAskBoolean(self, message):
		pass


