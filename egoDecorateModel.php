<?php

// egodecorate is a class that will act as a container for other classes and will send all calls
// to a buffer and will in the end send all calls to egolog, when getCalls gets called.



class egoDecorateModel
{
	protected $childClass;
	protected $arrAllCalls;

	public function __construct($childClass, $diffname)
	{
		$this->childClass = $childClass;
		$this->arrAllCalls = array();
		// register_shutdown_function( array($this, "getCalls" ));

	}

	public function __isset($name)
	{
		return isset($this->childClass->$name);
	}

	public function __get($name)
	{
		return $this->childClass->$name;
	}

	public function __set($name, $value)
	{
		$this->childClass->$name = $value;
	}

	public function __call($method_name, $args)
	{
		$this->arrAllCalls[] = $method_name;
		return call_user_func_array(array($this->childClass, $method_name), $args);
	}

	public function getCalls()
	{
		egolog($this->arrAllCalls, "egoDecorate:" . get_class($this->childClass));   
	}
}