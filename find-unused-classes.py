#!/usr/bin/python
import os
from os.path import isfile
import re

classes = {}
classesused = {}

def dicttodifffiller(path):
	files = os.listdir(path)
	for file in files:
		if isfile(path + "/" + file):
			f = open(path + "/" + file, 'r')
			klont = f.read()
			index = klont.find("class ")
			temp1 = klont[index:index + 50].split(" ")
			if len(temp1) > 1:
				classes.update({temp1[1]:{"file":file, "line":temp1[0] + " " + temp1[1], "charindex":index}})

			# index = klont.find("new ")
			for index in re.finditer('new |extends |implements ', klont):
				index = index.start()
				temp2 = klont[index:index + 50].split("(")[0].split(" ")			
				# print klont[index - 10:index + 50]
				if temp2[0] == "new ":
					temp = temp2[1]
				else:
					temp = temp2[1].split("\n")[0].replace('\r', '')

				if ',' not in temp and '$' not in temp and ';' not in temp and '\'' not in temp:
					# print temp
					classesused.update({temp:{"charindex":index,"file":file, "line":temp2[0] + " " + temp}})

			f.close()

paths = [
"/home/ank/public_html/naris3/models",
"/home/ank/public_html/naris3/models/DB",
"/home/ank/public_html/naris3/controllers",
"/home/ank/public_html/naris3/models/report"
]

for path in paths:
	dicttodifffiller(path)



for x in list(set(classes) - set(classesused)):
	print x
