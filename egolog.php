<?php

$fp = null;

// include_once 'Buffer.php';
include_once 'MessageDefs.php';


function egostart($port=5510, $host="localhost")
{

	global $fpegolog;
	//dont put a timeout on the fsockopen it will cause all kinds of mess
	//the problem here however is that if that the socket doesnt connect loading will take a long time

	$fpegolog = @fsockopen($host, $port);

}

function egolog($something, $tag = "default", $alsotrace = false)
{

	global $fpegolog;

	$messagedefs = new MessageDefs();
	$trace = "";
	if ($alsotrace) {
		$trace = " - " . print_r(debug_backtrace(), true) . " - ";
	}

	if (gettype($something) == "object" && (strpos(get_parent_class($something), "naris_db_row") !== false || strpos(get_class($something), "naris_db_row") || strpos(get_parent_class($something), "naris\\db\\") !== false)) {
		$something = (array) $something->getIterator();
	} elseif (gettype($something) == "array" && count($something, 1) > 5000) {
		$something = "this is a array with size " . count($something, 1) . " and will thus take too long !";
	}
	$message = $trace . print_r($something, true);
	
	$newbuff = $messagedefs->encodeMessageTag(array('message'=>$message, 'tag'=>$tag));

	try
	{
		fwrite($fpegolog,  $newbuff);
	}
	catch (Exception $e)
	{
		echo $e->getMessage();
	}

}

register_shutdown_function(
	function () 
	{
		global $fpegolog;
		@fclose($fpegolog);
	}
);
