<?php

include_once  'Buffer.php';

class MessageDefs 
{

    public $send_name = "aaps";

	public $inboundmessages = array();

	public function __construct()
    {
    	$this->inboundmessages += array(1=>'decodeMessageTag',2=>'decodeMd5Avalable',3=>'decodeAskNames',4=>'decodeAskBoolean');
    }

    public function handlemessage($buff)
    {
        $mesgtype = $buff->unpack('C');
        $this->send_name = $buff->unpack_uname();
        
        if (array_key_exists($mesgtype, $this->inboundmessages))
        {
            call_user_func($this->inboundmessages[$mesgtype], $buff);
        }
    }

    // DECODING

    public function decodeMessageTag($data=Null)
    {
        $data = $data->unpack_bzip_as_buffer();
    	$message = array("message"=>(string) $data->unpack_solid_string(), "tag"=>(string) $data.unpack_tag());
    	$this->handleMessageTag($message);
    }

    public function decodeMd5Avalable($data)
    {
        $message = array("md5"=>(string)($data->unpack_solid_string()));
        $this->handleMd5Avalable($message);
    }

    public function decodeAskNames($data)
    {
        $this->handleAskName();
    }

    public function decodeAskBoolean($data)
    {
        $whut = $data->unpack_num_bool();
        $message = array("mesg_num"=>(string)($num),"bool"=>(string)($boellie));
        $this->handleAskBoolean($message);
    }

    // ENCODING

    public function encodeMessageTag($data)
    {
    	$buff = new Buffer();
    	$newbuff = $buff->pack_solid_string($data['message'])->pack_tag($data['tag'])->pack_as_bzip_buffer()->add_uname($this->send_name)->add_mesg_num(1)->add_length()->add_checksum();
        return $newbuff;
    }

    public function encodeMd5Avalable($data)
    {
        $buff = new Buffer();
        $newbuff = $buff->pack_solid_string($data['md5'])->add_uname($this->send_name)->add_mesg_num(2)->add_length()->add_checksum();
    }

    public function encodeAskNames($data)
    {
        $buff = new Buffer();
        $newbuff = $buff->add_uname($this->send_name).add_mesg_num(3).add_length().add_checksum();
    }

    public function encodeAskBoolean($data)
    {
        $buff = new Buffer();
        $newbuff = $buff->pack_num_bool()->add_uname($this->send_name)->add_mesg_num(4)->add_length()->add_checksum();
    }

    // HANDELING

    public function handleMessageTag($message)
    {

    }

    public function handleMd5Avalable($message)
    {

    }

    public function handleAskBoolean($message)
    {

    }

    public function handleAskNames($message)
    {

    }
}