import struct
import json
import re
import base64
import bz2
import hashlib

class Buffer(object):
    buff = b""
    pos = 0

    def __init__(self, buffy=None):
        if buffy is not None:
            self.buff = buffy

    def __str__(self):
        return self.buff

    def __len__(self):
        return len(self.buff) - self.pos

    def add_from_connection(self, connection):
        rawlength = connection.recv(4)

        if not rawlength:
            return False
        length = struct.unpack('>I',rawlength)[0]
        self.buff = connection.recv(length)
        self.pos = 0

        check = connection.recv(32)
        m = hashlib.md5()
        m.update(self.buff)
        return check ==  m.hexdigest()


    def add_end(self, data):
        self.buff += data

    def add_begin(self, data):
        self.buff = data + self.buff

    def save(self):
        self.buff = self.buff[self.pos:]
        self.pos = 0

    def restore(self):
        self.pos = 0

    def discard(self):
        self.pos = len(self.buff)

    def read(self, length=None):
        if length is None:
            data = self.buff[self.pos:]
            self.pos = len(self.buff)
        else:
            if self.pos + length > len(self.buff):
                raise Exception("Buffer underrun !")
            data = self.buff[self.pos:self.pos+length]
            self.pos += length
        return data

    def unpack(self, fmt):
        fmt = ">"+fmt
        length = struct.calcsize(fmt)
        fields = struct.unpack(fmt, self.read(length))
        if len(fields) == 1:
            fields = fields[0]
        return fields

    def add_mesg_num(self, num):
        self.add_begin(self.pack('B', num))
        return self

    def unpack_solid_string_as_buffer(self):
        buff = Buffer()
        buff.add_end(self.buff)
        return buff

    def unpack_bzip_as_buffer(self):
        buff = Buffer()
        buff.add_end(bz2.decompress(self.read()))
        return buff

    def pack_as_bzip_buffer(self):
        buff = Buffer()
        buff.add_end(bz2.compress(self.buff))
        return buff
    

    def unpack_solid_string(self):
        buff = Buffer()
        length = self.unpack('I')
        buff.add_end(self.read(length))
        return buff

    def pack_solid_string(self, string):
        length = len(string)
        self.add_end(self.pack('I', length))
        self.add_end(string)
        return self

    def pack_num_bool(self, boellie, mesg_num):
        self.add_end(self.pack('H', (int(boellie) << 8) + mesg_num));
        return self

    def pack_colors(self, foreground, background):
        self.add_end(self.pack('H'), (foreground << 8) + background)
        return self

    def unpack_colors(self):
        buff = Buffer()
        combined = self.unpack('H');
        foreground = (combined >> 8) & 0xff
        background = combined & 0xff
        return foreground, background

    def unpack_num_bool(self):
        buff = Buffer()
        combined = self.unpack('H')
        boellie = (combined >> 8) & 0xff
        mesgnum = combined & 0xff
        return boellie, mesgnum


    def unpack_tag(self):
        buff = Buffer()
        length = self.unpack('B')
        buff.add_end(self.read(length))
        return buff

    def pack_tag(self, tag):
        length = len(tag);
        self.add_end(self.pack('B', length));
        self.add_end(tag);
        return self

    def pack_tags(self, tags):
        if type(tags) is list:
            return
        length = len(tags)
        self.add_end(self.pack('B', length))
        for tag in tags:
            self.pack_tag(tag)
        return self

    def add_uname(self, name):
        buff = Buffer()
        temp = str(buff.pack_uname(name))
        self.add_begin(temp)
        return self


    def pack_uname(self, uname):
        length = len(uname);
        self.add_end(self.pack('B', length))
        self.add_end(uname)
        return self

    def unpack_uname(self):
        buff = Buffer()
        length = self.unpack('B')
        buff.add_end(self.read(length))

        return buff

    def add_checksum(self):
        m = hashlib.md5()
        m.update(self.buff[4:])
        self.add_end(m.hexdigest())
        return self

    def add_length(self):
        self.add_begin(self.pack('N', strlen(self.buff)));


    @classmethod
    def pack(cls, fmt, *fields):
        return struct.pack(">"+fmt, *fields)






